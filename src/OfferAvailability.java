/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Viraji Amarajeewa
 */
public abstract class OfferAvailability implements Courses{
   protected Courses withOffer;

   public OfferAvailability(Courses withOffer){
      this.withOffer = withOffer;
   }

   public void issue(){
      withOffer.issue();
   }	
}
