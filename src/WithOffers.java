/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Viraji Amarajeewa
 */
public class WithOffers extends OfferAvailability{
    
   public WithOffers(Courses withOffer) {
      super(withOffer);		
   }

   @Override
   public void issue() {
      withOffer.issue();	       
      setOffers(withOffer);
   }

   private void setOffers(Courses withOffer){
      System.out.println("Offers are available");
   }
    
}
