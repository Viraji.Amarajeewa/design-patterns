/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Viraji Amarajeewa
 */
public class CoursesApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
      Courses course1 = new Hardware();
      Courses courseWithOffer1 = new WithOffers(new Hardware());
      Courses courseWithOffer2 = new WithOffers(new Software());
      
      System.out.println("Requested Courses");
      System.out.println("");
      course1.issue();

      System.out.println("");
      courseWithOffer1.issue();

      System.out.println("");
      courseWithOffer2.issue();
    }
}
    

